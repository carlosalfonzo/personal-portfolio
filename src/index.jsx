import { createRoot } from 'react-dom/client'
import React from 'react'
import './Fonts.css'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'

document.documentElement.style.setProperty('--height', `${window.screen.availHeight}px`)
const container = document.getElementById('root')
const root = createRoot(container) // createRoot(container!) if you use TypeScript

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
